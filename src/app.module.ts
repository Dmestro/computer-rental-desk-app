import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {EquipmentModule} from "./modules/equipment/equipment.module";
import {ConnectionOptions} from "typeorm";
import {TypeOrmModule} from "@nestjs/typeorm";
import {AuthModule} from "./modules/auth/auth.module";
import {UsersModule} from "./modules/users/users.module";
import {TypeOrmModuleOptions} from "@nestjs/typeorm/dist/interfaces/typeorm-options.interface";

export const connectionOptions: ConnectionOptions = {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'password',
    database: 'computer_rental_desk_db',
    dropSchema: false,
    entities: [
        __dirname + '/**/*.entity{.ts,.js}',
    ],
    synchronize: true,
};

export const herokuConnectionOptions: ConnectionOptions = {
    type: 'postgres',
    host: 'ec2-54-246-87-132.eu-west-1.compute.amazonaws.com',
    port: 5432,
    username: 'mjsqcztgchxkdw',
    password: '52046b3ce8914a8c6dcd9ef3b77cac731b274401bc993c1b3cb8675d21390473',
    database: 'd5udj2gdkq591q',
    entities: [
        __dirname + '/../**/*.entity.js',
    ],
    synchronize: true,
    ssl: true,
    extra: {
        ssl: {
            rejectUnauthorized: false,
        },
    },
};

@Module({
    imports: [
        AuthModule,
        UsersModule,
        EquipmentModule,
        TypeOrmModule.forRoot(connectionOptions)
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
