import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserDto } from './dto/user.dto';

import { CreateUserDto } from './dto/user.create.dto';
import { LoginUserDto } from './dto/user-login.dto';
import {UserEntity} from "./entity/user.entity";


import * as bcrypt from 'bcrypt';

export const toUserDto = (data: UserEntity): UserDto => {
  const { id, email, password, name, lastName, phone, address, birthDate, role} = data;

  let userDto: UserDto = {
    id,
    email,
    name,
    lastName,
    phone,
    address,
    birthDate,
    role
  };

  return userDto;
};

export const comparePasswords = async (userPassword, currentPassword) => {
  return await bcrypt.compare(currentPassword, userPassword);
};


@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepo: Repository<UserEntity>,
  ) {}

  async findOne(options?: object): Promise<UserDto> {
    const user = await this.userRepo.findOne(options);
    return toUserDto(user);
  }

  async findByLogin({ email, password }: LoginUserDto): Promise<UserDto> {
    const user = await this.userRepo.findOne({ where: { email: email } });

    if (!user) {
      throw new HttpException('Такого пользователя не существует. Пожалуйста, зарегистрируйтесь.', HttpStatus.UNAUTHORIZED);
    }

    // compare passwords
    const areEqual = await comparePasswords(user.password, password);

    if (!areEqual) {
      throw new HttpException('Неправильный логин или пароль.', HttpStatus.UNAUTHORIZED);
    }

    return toUserDto(user);
  }

  async findByPayload({ username }: any): Promise<UserDto> {
    return await this.findOne({ where: { username } });
  }

  async create(userDto: CreateUserDto): Promise<UserDto> {
    const { email, password, name, lastName, phone, address, birthDate } = userDto;

    // check if the user exists in the db
    const userInDb = await this.userRepo.findOne({ where: { email } });
    if (userInDb) {
      throw new HttpException('Пользователь с таким адресом электронной почты уже зарегистрирован', HttpStatus.BAD_REQUEST);
    }

    const user: UserEntity = await this.userRepo.create({
      email,
      password,
      name,
      lastName,
      phone,
      address,
      birthDate,
      role: "user"
    });

    await this.userRepo.save(user);

    return toUserDto(user);
  }

  private _sanitizeUser(user: UserEntity) {
    delete user.password;
    return user;
  }
}
