
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  BeforeInsert,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import {Exclude} from "class-transformer";

@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn('uuid') id: string;
  @Column({ nullable: false}) name: string;
  @Column({ nullable: false}) lastName: string;

  @Exclude()
  @Column({ type: 'varchar', nullable: false })
  password: string;
  @Column({ type: 'varchar', nullable: false ,unique: true }) email: string;

  @Exclude()
  @CreateDateColumn()
  createdOn?: Date;

  @Exclude()
  @CreateDateColumn()
  updatedOn?: Date;

  @Column() phone: string
  @Column() address: string

  @Column({nullable: true}) role: string

  @Column({
    type: "timestamp",
    default: () => 'LOCALTIMESTAMP'
  })
  birthDate: Date;

  @BeforeInsert()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }
}
