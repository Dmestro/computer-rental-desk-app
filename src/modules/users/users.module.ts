import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UserEntity } from './entity/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import {UsersController} from "./users.controller";
import {PublicUserServiceService} from "./public-user.service";

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity])],
  controllers: [UsersController],
  providers: [UsersService, PublicUserServiceService ],
  exports: [UsersService],
})
export class UsersModule {}
