import {Crud, CrudController} from "@nestjsx/crud";
import {UserEntity} from "./entity/user.entity";
import {Body, Controller, Param, Post} from "@nestjs/common";
import {EAGER, UUID_PARAMETER} from "../../constants";
import {PublicUserServiceService} from "./public-user.service";

@Crud({
    model: {
        type: UserEntity
    },
    params: {
        ...UUID_PARAMETER
    }
})
@Controller('user')
export class UsersController implements CrudController<UserEntity> {
    constructor(public service: PublicUserServiceService) {
    }

    @Post('update-role/:userId/')
    async updateUserRole(@Param() params, @Body() body: {role: string}): Promise<UserEntity> {
        const user = await this.service.getUserById(params.userId);

        user.role = body.role;

        return this.service.save(user);
    }
}
