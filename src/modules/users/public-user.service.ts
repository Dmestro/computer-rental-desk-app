import {Repository} from "typeorm";
import {TypeOrmCrudService} from "@nestjsx/crud-typeorm";
import {UserEntity} from "./entity/user.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {Injectable} from "@nestjs/common";



@Injectable()
export class PublicUserServiceService extends TypeOrmCrudService<UserEntity> {
    constructor(@InjectRepository(UserEntity) repo: Repository<UserEntity>) {
        super(repo);
    }

    public async getUserById(id: string): Promise<UserEntity> {
        return await this.repo.findOne({id: id})
    }

    public async save(user: UserEntity): Promise<UserEntity> {
        return this.repo.save(user);
    }
}
