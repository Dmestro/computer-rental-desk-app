

export interface LoginStatus {
  username: string;
  accessToken: any;
  expiresIn: any;
  role: string;
  id: string;
}
