import { Injectable } from '@nestjs/common';
import {TypeOrmCrudService} from "@nestjsx/crud-typeorm";
import {Rent} from "./rent.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {Equipment} from "../equipment/equipment.entity";
import {Repository} from "typeorm";

@Injectable()
export class RentService extends TypeOrmCrudService<Rent>{
    constructor(@InjectRepository(Rent) repo: Repository<Rent>) {
        super(repo);
    }
}
