import {Column, Entity, ManyToOne, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {UserEntity} from "../../users/entity/user.entity";
import {Equipment} from "../equipment/equipment.entity";

@Entity()
export class Rent {
    @PrimaryGeneratedColumn('uuid') id: string;

    @ManyToOne(type => UserEntity, u => u.id, {nullable: true})
    user: UserEntity;

    @ManyToOne(type => Equipment, e => e.id, {nullable: true})
    equipment: Equipment;

    @Column()
    rentTime: string


    @Column({
        type: "timestamp",
        default: () => 'LOCALTIMESTAMP'
    })
    startRentDate: Date;
}
