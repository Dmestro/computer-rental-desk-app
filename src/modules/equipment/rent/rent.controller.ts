import { Controller } from '@nestjs/common';
import {Crud, CrudController} from "@nestjsx/crud";
import {EAGER, UUID_PARAMETER} from "../../../constants";
import {Rent} from "./rent.entity";
import {RentService} from "./rent.service";

@Crud({
    model: {
        type: Rent
    },
    params: {
        ...UUID_PARAMETER
    },
    query: {
        join: {
            "equipment": EAGER,
            "user": EAGER
        }
    }
})
@Controller('rent')
export class RentController implements CrudController<Rent>{
    constructor(public service: RentService) {
    }
}
