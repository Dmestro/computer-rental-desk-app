import { Module } from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Equipment} from "./equipment/equipment.entity";
import {EquipmentService} from "./equipment/equipment.service";
import {EquipmentController} from "./equipment/equipment.controller";
import {EquipmentModel} from "./model/equipment-model.entity";
import {EquipmentType} from "./type/equipment-type.entity";
import {EquipmentModelService} from "./model/equipment-model.service";
import {EquipmentTypeService} from "./type/equipment-type.service";
import {EquipmentModelController} from "./model/equipment-model.controller";
import {EquipmentTypeController} from "./type/equipment-type.controller";
import {Manufacturer} from "./manufacturer/manufacturer.entity";
import {ManufacturerService} from "./manufacturer/manufacturer.service";
import {ManufacturerController} from "./manufacturer/manufacturer.controller";
import {RentService} from "./rent/rent.service";
import {RentController} from "./rent/rent.controller";
import {Rent} from "./rent/rent.entity";
import {Repair} from "./repair/repair.entity";
import {RepairService} from "./repair/repair.service";
import {RepairController} from "./repair/repair.controller";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Equipment,
            EquipmentModel,
            EquipmentType,
            Manufacturer,
            Rent,
            Repair
        ])
    ],
    providers: [
        EquipmentService,
        EquipmentModelService,
        EquipmentTypeService,
        ManufacturerService,
        RentService,
        RepairService
    ],
    controllers: [
        EquipmentController,
        EquipmentModelController,
        EquipmentTypeController,
        ManufacturerController,
        RentController,
        RepairController
    ]
})
export class EquipmentModule {}
