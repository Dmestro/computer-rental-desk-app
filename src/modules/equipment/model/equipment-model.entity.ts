import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Equipment} from "../equipment/equipment.entity";
import {EquipmentType} from "../type/equipment-type.entity";
import {Manufacturer} from "../manufacturer/manufacturer.entity";

@Entity()
export class EquipmentModel {
    @PrimaryGeneratedColumn('uuid') id: string;
    @Column() name: string;
    @Column("jsonb", {nullable: true})
    parameters: Object;

    @ManyToOne(type => Manufacturer, m => m.id ,{
        eager: true
    })
    manufacturer: Manufacturer;

    @ManyToOne(type => EquipmentType, et => et.id, {
        eager: true
    })
    type: EquipmentType;
}
