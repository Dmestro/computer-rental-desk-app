import { Controller } from '@nestjs/common';
import {Crud, CrudController} from "@nestjsx/crud";
import {EquipmentModel} from "./equipment-model.entity";
import {EquipmentModelService} from "./equipment-model.service";
import {EAGER, UUID_PARAMETER} from "../../../constants";

@Crud({
    model: {
        type: EquipmentModel
    },
    params: {
        ...UUID_PARAMETER
    },
    query: {
        join: {
            manufacturer: EAGER,
            type: EAGER
        }
    }

})
@Controller('equipment-models')
export class EquipmentModelController implements CrudController<EquipmentModel> {
    constructor(public service: EquipmentModelService) {
    }
}
