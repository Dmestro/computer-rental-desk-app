import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import {Repository} from "typeorm";
import {EquipmentModel} from "./equipment-model.entity";



@Injectable()
export class EquipmentModelService extends TypeOrmCrudService<EquipmentModel> {
    constructor(@InjectRepository(EquipmentModel) repo: Repository<EquipmentModel>) {
        super(repo);
    }
}
