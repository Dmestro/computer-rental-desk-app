import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import {Equipment} from "./equipment.entity";
import {Repository} from "typeorm";



@Injectable()
export class EquipmentService extends TypeOrmCrudService<Equipment> {
    constructor(@InjectRepository(Equipment) repo: Repository<Equipment>) {
        super(repo);
    }
}
