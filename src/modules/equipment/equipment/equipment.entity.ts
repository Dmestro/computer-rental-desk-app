import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import {EquipmentModel} from "../model/equipment-model.entity";

@Entity()
export class Equipment {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column({
        type: "timestamp",
        default: () => 'LOCALTIMESTAMP'
    })
    releaseDate: Date;

    @Column() inventoryNumber: string;
    @Column({default: true}) availability: boolean;
    @Column({nullable: true}) location: string;

    @ManyToOne(type => EquipmentModel, em => em.id , {
        eager:true
    })
    model: EquipmentModel;

    @Column({nullable: true}) rentPrice: number


}
