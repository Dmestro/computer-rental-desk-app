import { Controller } from '@nestjs/common';
import {Crud, CrudController} from "@nestjsx/crud";
import {Equipment} from "./equipment.entity";
import {EquipmentService} from "./equipment.service";
import {EAGER, UUID_PARAMETER} from "../../../constants";

@Crud({
    model: {
        type: Equipment
    },
    params: {
        ...UUID_PARAMETER
    },
    query: {
        join: {
            model: EAGER,
            "model.type": EAGER,
            "model.manufacturer": EAGER,
        }
    }
})
@Controller('equipment')
export class EquipmentController implements CrudController<Equipment> {
    constructor(public service: EquipmentService) {
    }
}
