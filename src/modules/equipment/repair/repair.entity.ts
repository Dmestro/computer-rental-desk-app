import {Column, Entity, ManyToOne, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {UserEntity} from "../../users/entity/user.entity";
import {Equipment} from "../equipment/equipment.entity";

@Entity()
export class Repair {
    @PrimaryGeneratedColumn('uuid') id: string;

    @ManyToOne(type => UserEntity, u => u.id)
    user: UserEntity;

    @ManyToOne(type => UserEntity, u => u.id, {nullable: true})
    serviceEngineer: UserEntity;

    @ManyToOne(type => Equipment, e => e.id)
    equipment: Equipment;

    @Column({nullable: true})
    price: number;

    @Column({nullable: true})
    comment: string;

    @Column({nullable: true})
    status: string;

    @Column({nullable: true})
    type: string;

    @Column({
        type: "timestamp",
        nullable: true,
    })
    endDate: Date;

    @Column({nullable: true})
    materials: string;
}
