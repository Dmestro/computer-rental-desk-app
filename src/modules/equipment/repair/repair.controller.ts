import { Controller } from '@nestjs/common';
import {Crud, CrudController} from "@nestjsx/crud";
import {EAGER, UUID_PARAMETER} from "../../../constants";
import {Repair} from "./repair.entity";
import {RepairService} from "./repair.service";

@Crud({
    model: {
        type: Repair
    },
    params: {
        ...UUID_PARAMETER
    },
    query: {
        join: {
            "serviceEngineer": EAGER,
            "user": EAGER,
            "equipment" : EAGER
        }
    }
})
@Controller('repair')
export class RepairController implements CrudController<Repair>{
    constructor(public service: RepairService) {
    }
}
