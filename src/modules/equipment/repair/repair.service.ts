import { Injectable } from '@nestjs/common';
import {TypeOrmCrudService} from "@nestjsx/crud-typeorm";
import {Repair} from "./repair.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";

@Injectable()
export class RepairService extends TypeOrmCrudService<Repair>{
    constructor(@InjectRepository(Repair) repo: Repository<Repair>) {
        super(repo);
    }
}
