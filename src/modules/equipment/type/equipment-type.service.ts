import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import {Repository} from "typeorm";
import {EquipmentType} from "./equipment-type.entity";



@Injectable()
export class EquipmentTypeService extends TypeOrmCrudService<EquipmentType> {
    constructor(@InjectRepository(EquipmentType) repo: Repository<EquipmentType>) {
        super(repo);
    }
}
