import {Column, Entity,  PrimaryGeneratedColumn} from "typeorm";


@Entity()
export class EquipmentType {
    @PrimaryGeneratedColumn('uuid') id: string;
    @Column() name: string;
}
