import { Controller } from '@nestjs/common';
import {Crud, CrudController} from "@nestjsx/crud";
import {EquipmentType} from "./equipment-type.entity";
import {EquipmentTypeService} from "./equipment-type.service";
import {UUID_PARAMETER} from "../../../constants";

@Crud({
    model: {
        type: EquipmentType,
    },
    params: {
        ...UUID_PARAMETER
    }
})
@Controller('equipment-types')
export class EquipmentTypeController implements CrudController<EquipmentType> {
    constructor(public service: EquipmentTypeService) {
    }
}
