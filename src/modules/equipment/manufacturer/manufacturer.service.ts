import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import {Manufacturer} from "./manufacturer.entity";
import {Repository} from "typeorm";



@Injectable()
export class ManufacturerService extends TypeOrmCrudService<Manufacturer> {
    constructor(@InjectRepository(Manufacturer) repo: Repository<Manufacturer>) {
        super(repo);
    }
}
