import { Controller } from '@nestjs/common';
import {Crud, CrudController} from "@nestjsx/crud";
import {Manufacturer} from "./manufacturer.entity";
import {ManufacturerService} from "./manufacturer.service";
import {UUID_PARAMETER} from "../../../constants";

@Crud({
    model: {
        type: Manufacturer
    },
    params: {
        ...UUID_PARAMETER
    }
})
@Controller('manufacturers')
export class ManufacturerController implements CrudController<Manufacturer> {
    constructor(public service: ManufacturerService) {
    }
}
