import {ParamsOptions} from "@nestjsx/crud";
import {JoinOption} from "@nestjsx/crud/lib/interfaces/query-options.interface";

export const UUID_PARAMETER: ParamsOptions = {
    id: {
        type: 'uuid',
        primary: true,
        field: 'id'
    }
};

export const EAGER: JoinOption = {eager: true};

export const SECRET_KEY = "myAWesomeSecretKey";
